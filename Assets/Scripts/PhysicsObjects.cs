﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PhysicsObjects : MonoBehaviour
{
    //note, this is the PennyPixel base so there are 
    //quite a few parts of it that I don't understand

    protected const float minMoveDistance = 0.001f;
    protected const float shellRadius = 0.01f;

    [HideInInspector] public Animator animator;
    protected ContactFilter2D contactFilter;
    protected Rigidbody2D rb2d;

    private float distance = 2f;
    [HideInInspector]  public float gravityModifier = 1f;
    protected bool grounded;
    protected Vector2 groundNormal;
    protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);
    [HideInInspector] public float minGroundNormalY = .65f;

    //audio
    [HideInInspector] public AudioSource audioSource;
    public AudioClip Collect;
    public AudioClip Win;
    public AudioClip Jump;
    public AudioClip Hit;

    //raycasting things
    private Vector2 ray;
    [HideInInspector] public float rpaikka;
    [HideInInspector] public float rpituus;
    private Vector2 startp;

    protected Vector2 targetVelocity;
    [HideInInspector] public Vector2 velocity;

    //counting health
    private int _health;
    public Text healthText;

    //counting pickups 
    private int _count;
    public Text textCount;

    //winning texts and image and UI elements
    public Text Congrats;
    public Text Grade;
    public Image End;
    public SpriteRenderer UiFlame;
    public SpriteRenderer UiHeart;

    //for waiting before death
    private IEnumerator coroutine;

    private void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        _count = 0;
        textCount.text = "COLLECTED: " + _count.ToString();

        _health = 3;
        healthText.text = "LIVES: " + _health.ToString();
    }

    private void Update()
    {
        //the current place of the player
        startp = transform.position;

        // raycasting thing
        var maski = 1 << 8;

        //¯\_(ツ)_/¯ 
        targetVelocity = Vector2.zero;
        ComputeVelocity();

        //drawing a line for debugging, not the real line!
        Debug.DrawRay(ray, new Vector2(rpituus, 0), Color.magenta);
        ray.y = transform.position.y;
        ray.x = transform.position.x - rpaikka;

        //this is the real line, but it's invisible, hence the other one
        var hit = Physics2D.Raycast(ray, new Vector2(1, 0), rpituus, maski);  
    }

    protected virtual void ComputeVelocity()
    {
        //alustaa controllerin sisällä olevan metodin
    }

    //no idea of any of this
    private void FixedUpdate()
    {
        velocity += gravityModifier * Physics2D.gravity * Time.deltaTime;
        velocity.x = targetVelocity.x;

        grounded = false;

        var deltaPosition = velocity * Time.deltaTime;

        var moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);

        var move = moveAlongGround * deltaPosition.x;

        Movement(move, false);

        move = Vector2.up * deltaPosition.y;

        Movement(move, true);
    }

    //nor this tbh 
    private void Movement(Vector2 move, bool yMovement)
    {
        distance = move.magnitude;

        if (distance > minMoveDistance)
        {
            var count = rb2d.Cast(move, contactFilter, hitBuffer, distance + shellRadius);
            hitBufferList.Clear();
            for (var i = 0; i < count; i++) hitBufferList.Add(hitBuffer[i]);

            for (var i = 0; i < hitBufferList.Count; i++)
            {
                var currentNormal = hitBufferList[i].normal;
                if (currentNormal.y > minGroundNormalY)
                {
                    grounded = true;
                    if (yMovement)
                    {
                        groundNormal = currentNormal;
                        currentNormal.x = 0;
                    }
                }

                var projection = Vector2.Dot(velocity, currentNormal);
                if (projection < 0) velocity = velocity - projection * currentNormal;

                var modifiedDistance = hitBufferList[i].distance - shellRadius;
                distance = modifiedDistance < distance ? modifiedDistance : distance;
            }
        }

        rb2d.position = rb2d.position + move.normalized * distance;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        //if hits the enemy's head 
        if (col.gameObject.CompareTag("EnemyTrig"))
        {
            //throws the player up a bit
            transform.DOLocalMoveY(startp.y + 1, 1);
            audioSource.PlayOneShot(Jump);
        }

        if (col.gameObject.CompareTag("Pickup"))
        {
            audioSource.PlayOneShot(Collect);
            col.gameObject.SetActive(false);
            _count = _count + 1;
            textCount.text = "COLLECTED: " + _count.ToString();
        }

        if (col.gameObject.CompareTag("Cross"))
        {
            StartCoroutine(Winning());
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //if falls into an eternal pit
        if (other.gameObject.CompareTag("Pit"))
        {
            _health = 0;
        }

        //if hits an enemy or an endEnemy
        if (other.gameObject.tag == "Enemy")
        {
            audioSource.PlayOneShot(Hit);
            //displays health
            _health = _health - 1;
            healthText.text = "LIVES: " + _health.ToString();

            //plays the hurt animation
            animator.SetBool("IsHurt", true);

            //throws the player back a bit
            transform.DOLocalMoveX(startp.x - 2, 1);
        }
        else
        {
            animator.SetBool("IsHurt", false);
        }

        //when dies
        if (_health == 0)
        {
            StartCoroutine(Death());
        }
    }

    IEnumerator Death()
    {
        
        rb2d.bodyType = RigidbodyType2D.Static;
        animator.SetBool("IsAlive", false);
        yield return new WaitForSeconds(2);


        var loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }

    IEnumerator Winning()
    {
        audioSource.PlayOneShot(Win);

        Congrats.GetComponent<Text>().enabled = true;
        End.GetComponent<Image>().enabled = true;
        UiFlame.GetComponent<SpriteRenderer>().enabled = false;
        UiHeart.GetComponent<SpriteRenderer>().enabled = false;
        healthText.GetComponent<Text>().enabled = false;
        textCount.GetComponent<Text>().enabled = false;

        if (_count == 0)
        {
            Grade.GetComponent<Text>().enabled = true;
            Grade.text = "F";
        }

        if (_count <= 4 && _count != 0)
        {
            Grade.GetComponent<Text>().enabled = true;
            Grade.text = "D";
        }

        if (_count >= 5 && _count < 8)
        {
            Grade.GetComponent<Text>().enabled = true;
            Grade.text = "C";
        }

        if (_count >= 8 && _count < 11)
        {
            Grade.GetComponent<Text>().enabled = true;
            Grade.text = "B";
        }

        if (_count >= 11 && _count < 15)
        {
            Grade.GetComponent<Text>().enabled = true;
            Grade.text = "A";
        }

        if (_count == 15)
        {
            Grade.GetComponent<Text>().enabled = true;
            Grade.text = "S";
        }

        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(0);
    }
}