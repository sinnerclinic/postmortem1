﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlagueController : PhysicsObjects {

    [HideInInspector] public float maxSpeed = 7;
    [HideInInspector] public float jumpTakeOffSpeed = 7;

    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Awake () 
    {
        spriteRenderer = GetComponent<SpriteRenderer> ();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Horizontal") && grounded)
        {
            animator.Play("Walk");
        }

        if (Input.GetButtonDown ("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
            audioSource.PlayOneShot(Jump);
        } else if (Input.GetButtonUp ("Jump")) 
        {
            if (velocity.y > 0) {
                velocity.y = velocity.y * 0.5f;
            }
        }

        if(move.x > 0.01f) //if moves right, turn sprite to the right
        {
            if(spriteRenderer.flipX == true)
            {
                spriteRenderer.flipX = false;
            }
        } 
        else if (move.x < -0.01f) //if moves left, turn sprite to the left
        {
            if(spriteRenderer.flipX == false)
            {
                spriteRenderer.flipX = true;
            }
        }

        animator.SetBool ("Grounded", grounded);
        animator.SetFloat ("Speed", Mathf.Abs (velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }
}
