﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Cinemachine;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public Animator anim;
    public Rigidbody2D rb2D;
    public Collider2D coll2D;

    private int _direction = -1;
    public float speed = 5f;
    

    // Update is called once per frame
    void Update()
    {

    }

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        coll2D = GetComponent<Collider2D>();
    }

    private void FixedUpdate()
    {
        rb2D.velocity = new Vector2(_direction * speed * Time.deltaTime, rb2D.velocity.y);
    }

     private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Wall")
        {
            ChangeDirection();
            Flipper();
        }
    }

    void ChangeDirection()
    {
        _direction *= -1;
    }

    void Flipper()
    {
        //transform.localScale = new Vector3(1, direction, 1);
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    
}
