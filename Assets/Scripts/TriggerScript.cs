﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    [SerializeField]

    #pragma warning disable 0649
    private EnemyScript _enemy;

    private Animator anim;


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            Destroy(_enemy.gameObject.GetComponent<BoxCollider>());

            _enemy.anim.SetBool("EnemyDeath", true);
            Destroy(_enemy.gameObject, 1);
        }
    }
}
